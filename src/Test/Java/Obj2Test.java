package Test.Java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

import Main.Java.Juego;



class Obj2Test {

	@Test
	void testDesapareceBola() {
		Juego game2 = new Juego();
		
		game2.o1.modPos(49,49); //hay que dar los valores antes de la colision a la pelota 1 en este test para comprobar la colisi�n
		game2.o2.modificarDireccion(49,49);//Por ello ponemos las mismas coordenadas, para que choquen
		game2.o2.desapareceBola();
		
		assertEquals(-50, game2.o2.getX());
	    assertEquals(-50, game2.o2.getY());//lo esperado es que se pinte la bola Obj2 en estas coordenadas como est� indicado en el m�todo desapareceBola
	}
	//Mis valores en el obj2 en 'x , y' son ambos positivos con valor de 1 para ambos.
	@Test
	void test2DesapareceBola() {
		Juego game2 = new Juego();
		game2.o2.modificarDireccion(0, 530); 
			game2.o2.desapareceBola();
		//Al tener y=530 y ser el l�mite de pared del m�todo desapareceBola  y al colisionar se le cambia la direcciones a cero y se queda quieta la pelota
			assertEquals(1, game2.o2.getX());
		    assertEquals(531, game2.o2.getY());/*Por ser mi eje x y mi eje y positivos con valor de 1, en los 
		    								     asserts como valor esperado tengo que sumar esos valores a los par�metros.*/
	}

}
