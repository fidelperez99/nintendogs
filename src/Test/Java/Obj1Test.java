package Test.Java;

import static org.junit.jupiter.api.Assertions.*;

import org.junit.Test;

import Main.Java.Juego;


class Obj1Test {

	@Test
	void testRecalculaPosicion() {
		Juego game = new Juego();
		game.o1.modPos(50, 50);
			game.o1.recalculaPosicion();
		    assertEquals(49, game.o1.getX());
		    assertEquals(51, game.o1.getY());
	}
	
	@Test
	void test2RecalculaPosicion() {
		Juego game = new Juego();
		game.o1.modPos(0, 50);
			game.o1.recalculaPosicion();//x=0 dir_x=-1 y=50 dir_y=1 Choca contra la pared 
			game.o1.recalculaPosicion();//x=-1 dir_x=1 y=51 dir_y=1 Y cambia la direccion de x
		    assertEquals(0, game.o1.getX());
		    assertEquals(52, game.o1.getY());
	}

}
