package Main.Java;

import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

//@author Jesús Gálvez Contreras

public class Obj3 {
	private int x=150;	//He puesto una posición donde el objeto colisione rápidamente con otro objeto.
	private int y=150;
	private Color c=Color.blue;	
	private Juego game;
		
	public Obj3(Juego game) {
		this.game= game;
	}
		
		
		//@param Graphics  le paso por parámetro los gráficos al método
		public void repinta(Graphics g) { 
			// Pinta el elemento en base a su posición, y otros parámetros
			Graphics2D g2d = (Graphics2D) g;
			g2d.setPaint(c);
			if(colision1()== true || colision2()== true)	//Si alguno de los métodos de colisión se vuelve true, cambia el objeto a color verde.
				c = Color.green;
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.fillOval(this.x, this.y, 30, 30);				
		}
		
		
		//@return boolean 
		public boolean colision1() {
			//Devuelve TRUE si este objeto entra en contacto con el o1.
			return this.game.o1.getBounds().intersects(getBounds());
		}
		//@return boolean
		public boolean colision2() {
			//Devuelve TRUE si este objeto entra en contacto con el o2.
			return this.game.o2.getBounds().intersects(getBounds());
		}
		
		
		public Rectangle getBounds() {
			//Definición de la "hitbox" del objeto.
			return new Rectangle(this.x, this.y, 30, 30);
		}
}
