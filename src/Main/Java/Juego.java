package Main.Java;
import java.awt.*;
import java.awt.Color;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import javax.swing.JFrame;
import javax.swing.JPanel;

public class Juego extends JPanel {
	/* Un comentario para forzar subida */
	public Obj1 o1 = new Obj1(this);
	public Obj2 o2 = new Obj2(this);
	public Obj3 o3 = new Obj3(this);

	public Juego() {
		addKeyListener(new KeyListener() {
			@Override
			public void keyTyped(KeyEvent e) {
			}

			@Override
			public void keyReleased(KeyEvent e) {
			}

			@Override
			public void keyPressed(KeyEvent e) {
				o1.teclaPresionada(e);
				o2.teclaPresionada(e);
			}
		});
		setFocusable(true);
	}

	private void recalculaPosiciones() {
		o1.recalculaPosicion();
		o2.recalculaPosicion();
	}

	@Override
	public void paint(Graphics g) {
		super.paint(g);
		o1.repinta(g);
		o2.repinta(g);
		o3.repinta(g);
	}

	public static void main(String[] args) throws InterruptedException {
		JFrame frame = new JFrame("Titulo");
		Juego game = new Juego();
		frame.add(game);
		frame.setSize(500, 600);
		frame.setVisible(true);
		frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

		while (true) {
			game.recalculaPosiciones();
			game.repaint();
			Thread.sleep(10);
		}
	}
}