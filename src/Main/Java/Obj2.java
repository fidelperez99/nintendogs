package Main.Java;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

public class Obj2 {
	private int x=0;
	private int y=0;
	private int dir_x=1;
	private int dir_y=1;
	private static final int WITH = 30;
	private static final int HEIGH = 30;	
	
	private Juego game;
	
	public Obj2(Juego game) {
		this.game= game;
	}
	//Y TECHOS
	//X PAREDES
	
	public void teclaPresionada(KeyEvent e) {
	

	
		
		//de esta manera no sigue pintando la bola fuera de los bordes, sin esta condici�n pintar�a la bola fuera y podr�a volver a aparecer
		if(desapareceBola()==false) {
			
			// es mover a la izquierda pero con la letra A
			if (e.getKeyCode() == KeyEvent.VK_A)
				dir_x = -1;
			// es mover a la derecha pero con la letra D
			if (e.getKeyCode() == KeyEvent.VK_D)
				dir_x = 1;
		
			// muevo hacia arriba con la tecla w
			if (e.getKeyCode() == KeyEvent.VK_W)
				dir_y = -1;
			//muevo hacia abajo con la tecla s
			if (e.getKeyCode() == KeyEvent.VK_S)
				dir_y = 1;
		}
		//Al desaparecer la bola, con la letra F consigo que vuelva a aparecer en las coordenadas indicadas abajo.
		if(desapareceBola()==true && e.getKeyCode()==KeyEvent.VK_F) {
			this.x=30;
			this.y=30;
			
		}
	}
	
	
	
	public boolean desapareceBola() {//desaparece la bola cuando toca un borde
		this.x=this.x+dir_x;
		this.y=this.y+dir_y;	
		if (colision()) {
			dir_y = dir_y * 0;
			dir_x = dir_x * 0;
		}
		//HE A�ADIDO PAREDES HA IZ, DER, ARRIBA Y ABAJO
		if ((this.x < 0) || (this.x > 450)) {
			System.out.println("resultado1");//he metido estos resultados para comprobar en consola por donde entraba los m�todos 
			return true;
		
		}
		
		else if ((this.y < 0) || (this.y > 530)) {
			System.out.println("resultado2");
			return true;
		
		}
		
		else if(this.game.o1.getBounds().intersects(getBounds())) {
			System.out.println("resultado3");
			this.x=-50;
			this.y=-50;//AL PONER EST�S POSICIONES IMPIDO QUE EL OBJ2 vuelva a aparecer en la ventana y la letra F cobre sentido
			return true;
		}
			
		else {
			return false;
		}
	}
	
	
	public void recalculaPosicion() {
		// recalcula posici�n en base a entradas y parametros
		this.x=this.x+dir_x;
		this.y=this.y+dir_y;		
	}


	//creo estos m�todos para poder asignar los par�metros de direcci�n de las bolas en la pruebas de unitarias creadas en la clase Obj2Test
	
	public void modificarDireccion(int x, int y) {
		this.x=x;
		this.y=y;
	}
	

	

	public void repinta(Graphics g) {
		// Pinta el elemento en base a su posici�n, y otros parametros
		Graphics2D g2d = (Graphics2D) g;	
		
		//Este m�todo ahora hace que al tocar la pared no pinta m�s la pelota
		if (desapareceBola() == false) {
			g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,

			RenderingHints.VALUE_ANTIALIAS_ON);
			g2d.fillOval(this.x, this.y, 30, 30);
		}
	}
	

	//Modifico la posici�n para las pruebas
	public int getX() {
		return this.x;
	}
	//Modifico la posici�n para las pruebas
	public int getY() {
		return this.y;
	}
	
	
	private boolean colision() {
		// indica que el cuadro de o1 intersecta con el mio
		return this.game.o1.getBounds().intersects(getBounds());
	}
	
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, this.WITH, this.HEIGH);
	}
}
