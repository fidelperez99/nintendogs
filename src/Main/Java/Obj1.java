package Main.Java;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.awt.Rectangle;
import java.awt.RenderingHints;
import java.awt.event.KeyEvent;

/*
 *@author Fidel P�rez
 */
public class Obj1 {
	private int x=450;
	private int y=1;
	private int dir_x=-1;
	private int dir_y=1;
	private static final int WITH = 30;
	private static final int HEIGH = 30;
	private Color c=Color.blue;
	private Juego game;
	
	/*
	 * @param le paso por parametros el juego
	 */
	public Obj1(Juego game) {
		this.game= game;
	}
	
	public void teclaPresionada(KeyEvent e) {
		//MOVIMIENTO DE LA BOLA HACIA IZQ
		if (e.getKeyCode() == KeyEvent.VK_LEFT)
			dir_x = -1;
		//MOVIMIENTO DE LA BOLA HACIA DER
		if (e.getKeyCode() == KeyEvent.VK_RIGHT)
			dir_x = 1;
		//MOVIMIENTO DE LA BOLA HACIA ARRIBA
		if (e.getKeyCode() == KeyEvent.VK_UP)
			dir_y = -1;
		//MOVIMIENTO DE LA BOLA HACIA ABAJO
		if (e.getKeyCode() == KeyEvent.VK_DOWN)
			dir_y = 1;
		//DETENCI�N BOLA
		if (e.getKeyCode() == KeyEvent.VK_SPACE) {
			dir_y = 0;
			dir_x = 0;
		}
		//MOVIMIENTO DIAGONAL IZQ Y ARRIBA
		if (e.getKeyCode() == KeyEvent.VK_O) {
			dir_y = -1;
			dir_x = -1;
		}
		//MOVIMIENTO DIAGONAL DER Y ARRIBA
		if (e.getKeyCode() == KeyEvent.VK_P) {
			dir_y = -1;
			dir_x = 1;
		}
		//MOVIMIENTO DIAGONAL IZQ Y ABAJO
		if (e.getKeyCode() == KeyEvent.VK_K) {
			dir_y = 1;
			dir_x = -1;
		}
		//MOVIMIENTO DIAGONAL DER Y ABAJO
		if (e.getKeyCode() == KeyEvent.VK_L) {
			dir_y = 1;
			dir_x = 1;
		}
	}
	
	public void recalculaPosicion() {
		// recalcula posición en base a entradas y parametros
		this.x=this.x+dir_x;
		this.y=this.y+dir_y;	
		if (colision()) {
			dir_y = dir_y * -1;
			dir_x = dir_x * -1;
		}
		//HE AÑADIDO PAREDES HA IZ, DER, ARRIBA Y ABAJO
		if ((this.x < 0) || (this.x > 450)) {
			dir_x = dir_x * -1;
		}else if ((this.y < 0) || (this.y > 530)) {
			dir_y = dir_y * -1;
		}
	}
	
	//Modifico la posición para las pruebas
	public void modPos(int x, int y) {
		this.x=x;
		this.y=y;
	}
	//@return Devuelvo X
	public int getX() {
		return this.x;
	}
	//@return Devuelvo Y
	public int getY() {
		return this.y;
	}
	
	public void repinta(Graphics g) {
		// Pinta el elemento en base a su posición, y otros parametros
		Graphics2D g2d = (Graphics2D) g;
		g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING,
				RenderingHints.VALUE_ANTIALIAS_ON);
		g2d.fillOval(this.x, this.y, 30, 30);	
	}
	
	private boolean colision() {
		// indica que el cuadro de o2 intersecta con el mio
		return this.game.o2.getBounds().intersects(getBounds());
	}
	
	public Rectangle getBounds() {
		return new Rectangle(this.x, this.y, this.WITH, this.HEIGH);
	}
	
}
